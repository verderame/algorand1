CREATE TABLE fund
(
    appid VARCHAR(20) NOT NULL,
    name VARCHAR(50) NOT NULL,
    description VARCHAR(50) NOT NULL,
    escrow VARCHAR(50) NOT NULL,
    startd VARCHAR(50) NOT NULL,
    endd VARCHAR(50) NOT NULL,
    PRIMARY KEY (appid)
);

