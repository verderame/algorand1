package com.controllers;

import com.entities.Fund;
import com.services.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping
@RestController
public class Controller {

    @Autowired
    private Service service;

    @GetMapping
    public ResponseEntity<List<Fund>> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @GetMapping(path = "{appId}")
    public ResponseEntity<Fund> get(@PathVariable String appId) {
        return new ResponseEntity<>(service.get(appId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Fund> create(@RequestBody Fund fund) {
        // backend should check blockchain activity (read global state of the app) before storing new fund info
        return new ResponseEntity<>(service.create(fund), HttpStatus.CREATED);
    }

}
