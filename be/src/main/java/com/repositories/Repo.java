package com.repositories;

import com.entities.Fund;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Repo extends JpaRepository<Fund, String> {

}
