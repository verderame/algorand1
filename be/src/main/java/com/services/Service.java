package com.services;

import com.entities.Fund;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    @Autowired
    private com.repositories.Repo repo;

    public List<Fund> getAll() {
        return repo.findAll();
    }

    public Fund get(String appid) {
        return repo.findById(appid).get();
    }

    public Fund create(Fund fund) {
        return repo.save(fund);
    }

}
