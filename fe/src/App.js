import React from "react";
import "./App.css";
import routes from "./routes";
import { Route, Routes } from "react-router-dom";
import HomePage from "./container/HomePage";

import "primereact/resources/themes/lara-dark-teal/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css"; //icons
import "primeflex/primeflex.css"; //advanced css
// import UserContext from "./store/user-context";

function App() {
  const getRoutes = (allRoutes) =>
    allRoutes.map((route) => {
      if (route.collapse) {
        return getRoutes(route.collapse);
      }

      if (route.route) {
        return (
          <Route
            exact
            path={route.route}
            element={route.component}
            key={route.key}
          />
        );
      }

      return null;
    });

  return (
    <Routes>
      {/* <UserContext.Provider value={{ mnemonicPhrase: "" }}> */}
      {getRoutes(routes)}
      <Route path="/" element={<HomePage />} />
      {/* </UserContext.Provider> */}
    </Routes>
  );
}

export default App;
