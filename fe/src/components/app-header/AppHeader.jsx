import React, { useState } from "react";
import "./AppHeader.css";
import { Menu } from "primereact/menu";
import { Sidebar } from "primereact/sidebar";
import { Button } from "primereact/button";
import { ToggleButton } from "primereact/togglebutton";
import { InputText } from "primereact/inputtext";
import { Tag } from "primereact/tag";
import TextInputDialog from "../text-input-dialog/TextInputDialog";
import { useEffect } from "react";

const AppHeader = (props) => {
  const { mnemonicPhrase, setMnemonicPhrase } = props;
  const [visibleMenu, setVisibleMenu] = useState(false);
  const [visibleDialog, setVisibleDialog] = useState(false);

  useEffect(() => {
    let mp = localStorage.getItem("memo");
    if (!mnemonicPhrase && mp && mp != "undefined") setMnemonicPhrase(mp);
  }, []);

  const items = [
    {
      label: "Home",
      url: "/",
    },
    {
      label: "About",
      url: "/about-us",
    },
    {
      label: "Projects",
      url: "/projects",
    },
    {
      label: "New Project",
      url: "/new-project",
    },
  ];
  return (
    <div className="app-header">
      <div className="custom-menubar">
        <div style={{ display: "flex" }}>
          <img
            alt="logo"
            src={require("./../../assets/whatif_logo.png")}
            onError={(e) =>
              (e.target.src =
                "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
            }
            height="40"
            className="header-logo"
          ></img>
          <div className="app-name">
            <span className="teal-text">What</span>
            <span className="light-teal-text">if</span>
          </div>
          <div className="flex align-items-center mx-3">
            <Tag severity="info" value="Alpha" rounded={true} />
          </div>
        </div>
        <div className="flex align-items-center">
          <div className="connect-wallet-wrapper">
            <Button
              className="header-button"
              label="Connect Wallet"
              onClick={() => setVisibleDialog(true)}
              icon="pi pi-wallet"
            />
          </div>
          <Sidebar
            className="custom-fs-menu"
            visible={visibleMenu}
            position="top"
            fullScreen={false}
            onHide={() => setVisibleMenu(false)}
          >
            <div className="fs-menu-container">
              <Menu model={items} />
            </div>
          </Sidebar>
          {/* <InputText
            placeholder="Insert your mnemonic phrase"
            value={mnemonicPhrase}
            onChange={(e) => setMnemonicPhrase(e.target.value)}
          /> */}
          <ToggleButton
            className="header-button p-button-text"
            checked={visibleMenu}
            onChange={(e) => setVisibleMenu(e.value)}
            onLabel=""
            offLabel=""
            onIcon="pi pi-times"
            offIcon="pi pi-bars"
            aria-label="Menu"
          />
        </div>
      </div>
      <TextInputDialog
        onSubmit={() => {
          localStorage.setItem("memo", mnemonicPhrase);
          setVisibleDialog(false);
        }}
        close={() => setVisibleDialog(false)}
        visible={visibleDialog}
        title="Save Your Mnemonic Phrase"
        inputValue={mnemonicPhrase}
        onChangeInput={(e) => setMnemonicPhrase(e.target.value)}
      />
    </div>
  );
};

export default AppHeader;
