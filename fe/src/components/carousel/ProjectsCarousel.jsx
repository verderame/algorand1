import React from "react"
import "./ProjectsCarousel.css"

import { Carousel } from "primereact/carousel"
import { Card } from "primereact/card"
import { Button } from "primereact/button"
import { Avatar } from "primereact/avatar"
import { Tag } from "primereact/tag"

import { useNavigate } from "react-router-dom"

import { avatar_1, avatar_2, avatar_3 } from "../../assets/img/avatars/index.js"
import optIn from "../../services/donate"

const ProjectsCarousel = (props) => {
  const { projectList } = props
  const avatars = [avatar_1, avatar_2, avatar_3]
  const navigate = useNavigate()

  const navigateProjects = () => {
    // 👇️ navigate to /
    navigate("/projects")
  }

  const responsiveOptions = [
    {
      breakpoint: "1400px",
      numVisible: 3,
      numScroll: 1,
    },
    {
      breakpoint: "1200px",
      numVisible: 2,
      numScroll: 1,
    },
    {
      breakpoint: "960px",
      numVisible: 1,
      numScroll: 1,
    },
  ]

  const itemTemplate = (item) => {
    const header = (
      <div className="relative card-img-container">
        <img className="carousel-card-img" alt={item.title} src={item.imgUrl} />
        <div className="absolute right-0 top-0 carousel-card-tag-container">
          <Tag
            className="carousel-card-tag"
            severity={item.severity}
            value={
              item.severity == "success"
                ? "Opened"
                : item.severity == "warning"
                ? "Closing"
                : "Closed"
            }
          ></Tag>
        </div>
      </div>
    )
    const footer = (
      <span>
        {/* countdown component */}
        <Button
          label="Donate"
          icon="pi pi-heart"
          className={
            item.severity == "danger" ? "p-button-outlined" : "p-button-primary"
          }
          // onClick={()=>optIn(appId, senderMnemonicPhrase, receiverMnemonicPhrase)}
          disabled={item.severity == "danger"}
        />
      </span>
    )
    return (
      <Card footer={footer} header={header} className="carousel-card">
        <div className="absolute border-circle avatar-container">
          <div className="avatar-wrapper">
            <Avatar
              image={avatars[item.avatarUrl]}
              className="border-round-sm"
              size="xlarge"
              shape="circle"
            />
          </div>
        </div>
        <div className="carousel-card-title">{item.title}</div>
        <div className="carousel-card-subtitle">{item.subtitle}</div>
        <div className="carousel-card-description">{item.description}</div>
      </Card>
    )
  }

  return (
    <div id="latest">
      <div className="flex justify-content-between align-items-center">
        <h1 className="p-6">Latest on Whatif</h1>
        <div className="pr-6">
          <Button
            label="See all"
            type="button"
            className="p-button-outlined"
            onClick={navigateProjects}
          />
        </div>
      </div>
      <div>
        <Carousel
          value={projectList}
          itemTemplate={itemTemplate}
          numVisible={3}
          numScroll={1}
          circular={true}
          responsiveOptions={responsiveOptions}
        />
      </div>
    </div>
  )
}

export default ProjectsCarousel
