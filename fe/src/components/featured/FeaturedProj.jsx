import React from "react"
import "./FeaturedProj.css"
import { Card } from "primereact/card"
import { Avatar } from "primereact/avatar"

import { avatar_1, avatar_2, avatar_3 } from "../../assets/img/avatars/index.js"

const FeaturedProj = (props) => {
  const avatars = [avatar_1, avatar_2, avatar_3]
  const { project } = props

  return (
    <div className="py-7">
      <div className="flex justify-content-between align-items-center">
        <h1 className="p-6">Featured Project</h1>
      </div>
      <div className="px-6">
        <Card className="featured-card">
          <div className="featured-card-bg">
            <div className="flex align-content-end flex-wrap h-30rem p-5">
              <div className="flex align-items-center ">
                <div className="border-circle featured-avatar-wrapper">
                  <Avatar
                    image={avatars[project.avatarUrl]}
                    className="border-round-sm"
                    size="xlarge"
                    shape="circle"
                  />
                </div>
                <div className="ml-5">
                  <div className="featured-card-title">{project.title}</div>
                  <div className="featured-card-subtitle">
                    {project.subtitle}
                  </div>
                  <div className="featured-card-description">
                    {project.description}
                  </div>
                </div>
                <div className="ml-5">{/* countdown component */}</div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default FeaturedProj
