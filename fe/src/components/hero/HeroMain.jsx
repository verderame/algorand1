import React from "react"
import { Button } from "primereact/button"
import "./HeroMain.css"
import { hero_img_1 } from "../../assets/img/hero/index.js"

const HeroMain = (props) => {
  return (
    <div className="grid grid-nogutter text-800">
      <div className="col-12 md:col-6 p-6 text-center md:text-left flex align-items-center ">
        <section>
          <span className="block text-6xl font-bold mb-1">For anyone</span>
          <div className="text-6xl text-primary font-bold mb-3">
            For everything
          </div>
          <p className="mt-0 mb-4 text-700 line-height-3">
            First state-of-the-art crowdfunding platform based on Algorand.
          </p>

          {/* <Button
            label="Latest projects"
            type="button"
            className="mr-3 p-button-raised"
          /> */}
          <a href="#latest" className="anchor-link">
            <Button
              label="Latest projects"
              type="button"
              className="p-button-outlined"
            />
          </a>
        </section>
      </div>
      <div className="col-10 md:col-6">
        <img src={hero_img_1} alt="hero-1" className=" md:h-full" style={{}} />
      </div>
    </div>
  )
}

export default HeroMain
