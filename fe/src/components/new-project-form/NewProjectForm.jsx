import React from "react";
import "./NewProjectForm.css";
import { InputText } from "primereact/inputtext";
import { InputNumber } from "primereact/inputnumber";
import { InputTextarea } from "primereact/inputtextarea";
import { Calendar } from "primereact/calendar";
import { ProgressBar } from "primereact/progressbar";

const NewProjectForm = (props) => {
  const { projectData, setProjectData, disabled } = props;

  const displayValueTemplate = (value) => {
    return (
      <React.Fragment>
        {260000000}/<b>{projectData.fundraiseGoal} ALGO</b>
      </React.Fragment>
    );
  };

  return (
    <div className="grid m-5">
      <div className="col-12 text-primary font-bold">
        <h1 className="text-center py-4">
          {disabled ? "Project Detail" : "New Project"}
        </h1>
      </div>

      <span
        className={`col-12 ${!disabled ? "md:col-6" : ""} new-project-input`}
      >
        <label htmlFor="title">Title</label>
        <InputText
          id="title"
          disabled={disabled}
          value={projectData.title}
          onChange={(e) =>
            setProjectData({ ...projectData, title: e.target.value })
          }
        />
      </span>

      {disabled ? (
        <span className="col-12  new-project-input">
          <label htmlFor="progress">Fundraise Progress</label>
          <ProgressBar
            id="progress"
            value={(260000000 * 100) / projectData.fundraiseGoal}
            displayValueTemplate={displayValueTemplate}
          />
        </span>
      ) : (
        <span className="col-12 md:col-6 new-project-input">
          <label htmlFor="fundraiseGoal">Fundraise Goal</label>
          <InputNumber
            id="fundraiseGoal"
            suffix=" MALGO"
            disabled={disabled}
            value={projectData.fundraiseGoal}
            onChange={(e) =>
              setProjectData({
                ...projectData,
                fundraiseGoal: e.value,
              })
            }
          />
        </span>
      )}
      <span className="col-12 new-project-input">
        <label htmlFor="description">Description</label>
        <InputTextarea
          id="description"
          disabled={disabled}
          value={projectData.description}
          onChange={(e) =>
            setProjectData({
              ...projectData,
              description: e.target.value,
            })
          }
        />
      </span>
      <span className="col-12 md:col-6 new-project-input">
        <label htmlFor="fundraiseStartDate">Fundraise Start Date</label>
        <Calendar
          id="fundraiseStartDate"
          value={projectData.fundraiseStartDate}
          dateFormat="dd/mm/yy"
          disabled={disabled}
          onChange={(e) =>
            setProjectData({
              ...projectData,
              fundraiseStartDate: e.value,
              fundraiseEndDate: undefined, // clear end dates when changed to ensure the correct timeline
              projectEndDate: undefined,
            })
          }
          // minDate={new Date()} // today
        />
      </span>
      <span className="col-12 md:col-6 new-project-input">
        <label htmlFor="fundraiseEndDate">Fundraise End Date</label>
        <Calendar
          id="fundraiseEndDate"
          value={projectData.fundraiseEndDate}
          dateFormat="dd/mm/yy"
          disabled={!projectData.fundraiseStartDate || disabled}
          onChange={(e) =>
            setProjectData({
              ...projectData,
              fundraiseEndDate: e.value,
              projectEndDate: undefined, // clear project end date when changed to ensure the correct timeline
            })
          }
          minDate={projectData.fundraiseStartDate}
        />
      </span>
      <span className="col-12 md:col-6 new-project-input">
        <label htmlFor="projectEndDate">Project End Date</label>
        <Calendar
          id="projectEndDate"
          value={projectData.projectEndDate}
          dateFormat="dd/mm/yy"
          disabled={!projectData.fundraiseEndDate || disabled}
          onChange={(e) =>
            setProjectData({
              ...projectData,
              projectEndDate: e.value,
            })
          }
          minDate={projectData.fundraiseEndDate}
        />
      </span>
      <span className="col-12 md:col-6 new-project-input">
        <label htmlFor="address">{"Address (reciever's public key)"}</label>
        <InputText
          id="address"
          disabled={disabled}
          value={projectData.address}
          onChange={(e) =>
            setProjectData({ ...projectData, address: e.target.value })
          }
        />
      </span>
      {disabled && (
        <span className="col-12 md:col-6 new-project-input">
          <label htmlFor="escrow">{"Escrow"}</label>
          <InputText
            id="escrow"
            disabled={disabled}
            value={projectData.escrow}
            onChange={(e) =>
              setProjectData({ ...projectData, escrow: e.target.value })
            }
          />
        </span>
      )}
    </div>
  );
};

export default NewProjectForm;
