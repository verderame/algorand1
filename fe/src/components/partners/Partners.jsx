import React from "react"
import { Skeleton } from "primereact/skeleton"
import { masterz_logo, algorand_logo } from "../../assets/img/partners/index"

const Partners = () => {
  return (
    <div className="py-7">
      <div className="pb-3 flex justify-content-center align-items-center">
        <div className="text-muted">Partners</div>
      </div>
      <div className="flex justify-content-center align-items-center my-3">
        <Skeleton
          className="mx-2 sm:mx-2 md:mx-5 w-7rem"
          height="3rem"
        ></Skeleton>
        <Skeleton
          className="mx-2 sm:mx-2 md:mx-5 w-7rem"
          height="3rem"
        ></Skeleton>
        <img
          className="mx-2 sm:mx-2 md:mx-5 max-w-7rem"
          src={masterz_logo}
          alt="masterz"
        />
        <img
          className="mx-2 sm:mx-2 md:mx-5 max-w-7rem"
          src={algorand_logo}
          alt="algorand"
        />
        <Skeleton
          className="mx-2 sm:mx-2 md:mx-5 w-7rem"
          height="3rem"
        ></Skeleton>
        <Skeleton
          className="mx-2 sm:mx-2 md:mx-5 w-7rem"
          height="3rem"
        ></Skeleton>
      </div>
    </div>
  )
}

export default Partners
