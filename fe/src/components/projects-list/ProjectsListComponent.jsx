import React, { useState, useEffect, useRef } from "react";
import { DataView, DataViewLayoutOptions } from "primereact/dataview";
import { ProductService } from "../../service/ProductService";
import { Button } from "primereact/button";
import { Card } from "primereact/card";
import { Avatar } from "primereact/avatar";
import { Tag } from "primereact/tag";
import "./DataViewDemo.css";
import "./ProjectsListComponent.css";
import "./../carousel/ProjectsCarousel.css";
import "../carousel/ProjectsCarousel.css";
import {
  avatar_1,
  avatar_2,
  avatar_3,
} from "../../assets/img/avatars/index.js";
import moment from "moment";
import { useNavigate } from "react-router-dom";

const ProjectsListComponent = (props) => {
  const { loading, products, goToDetail } = props;

  const [layout, setLayout] = useState("grid");
  const [first, setFirst] = useState(0);
  const [totalRecords, setTotalRecords] = useState(0);
  const rows = useRef(6);
  // const datasource = useRef(null);
  // const isMounted = useRef(false);
  // const productService = new ProductService();

  const avatars = [avatar_1, avatar_2, avatar_3];

  // useEffect(() => {
  //   if (isMounted.current) {
  //     setTimeout(() => {
  //       setLoading(false);
  //     }, 1000);
  //   }
  // }, [loading]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    console.log(products);
    // setTimeout(() => {
    //   isMounted.current = true;
    //   productService.getProducts().then((data) => {
    //     datasource.current = data;
    //     setTotalRecords(data.length);
    //     setProducts(datasource.current.slice(0, rows.current));
    //   });
    // }, 1000);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onPage = (event) => {
    //imitate delay of a backend call
    // setTimeout(() => {
    const startIndex = event.first;
    const endIndex = Math.min(event.first + rows.current, totalRecords - 1);
    // const newProducts =
    //   startIndex === endIndex
    //     ? datasource.current.slice(startIndex)
    //     : datasource.current.slice(startIndex, endIndex);

    setFirst(startIndex);
    // setProducts(newProducts);
    // setLoading(false);
    // }, 1000);
  };

  /* const renderListItem = (data) => {
    return (
      <div className="col-12">
        <div className="product-list-item">
          <img
            src={`images/product/${data.image}`}
            onError={(e) =>
              (e.target.src =
                "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
            }
            alt={data.name}
          />
          <div className="product-list-detail">
            <div className="product-name">{data.name}</div>
            <div className="product-description">{data.description}</div>
            <Rating value={data.rating} readOnly cancel={false}></Rating>
            <i className="pi pi-tag product-category-icon"></i>
            <span className="product-category">{data.category}</span>
          </div>
          <div className="product-list-action">
            <span className="product-price">${data.price}</span>
            <Button
              icon="pi pi-shopping-cart"
              label="Add to Cart"
              disabled={data.inventoryStatus === "OUTOFSTOCK"}
            ></Button>
            <span
              className={`product-badge status-${data.inventoryStatus.toLowerCase()}`}
            >
              {data.inventoryStatus}
            </span>
          </div>
        </div>
      </div>
    )
  } */

  const renderGridItem = (item) => {
    const header = (
      <div className="relative card-img-container">
        <img className="carousel-card-img" alt={item.title} src={item.imgUrl} />
        <div className="absolute right-0 top-0 carousel-card-tag-container">
          <Tag
            className="carousel-card-tag"
            severity={item.severity}
            value={
              item.severity == "success"
                ? "Opened"
                : item.severity == "warning"
                ? "Closing"
                : "Closed"
            }
          ></Tag>
        </div>
      </div>
    );
    const footer = (
      <span>
        {/* countdown component */}
        <Button
          label="Detail"
          icon="pi pi-eye"
          className={
            item.severity == "danger" ? "p-button-outlined" : "p-button-primary"
          }
          onClick={() => goToDetail(item.appId)}
          disabled={item.severity == "danger"}
        />
      </span>
    );
    return (
      <div className="col-12 md:col-4">
        <Card
          footer={footer}
          header={header}
          className="carousel-card flex flex-column"
        >
          <div className="absolute border-circle avatar-container">
            <div className="avatar-wrapper">
              <Avatar
                image={avatars[item.avatarUrl]}
                className="border-round-sm"
                size="xlarge"
                shape="circle"
              />
            </div>
          </div>
          <div className="carousel-card-title">{item.name}</div>
          <div className="carousel-card-subtitle">{item.subtitle}</div>
          <div className="carousel-card-description">{item.description}</div>
        </Card>
      </div>
    );
  };

  const itemTemplate = (product, layout) => {
    if (!product) {
      return;
    }

    /* if (layout === "list") return renderListItem(product)
    else */ if (
      layout === "grid"
    )
      return renderGridItem(product);
  };

  /* const renderHeader = () => {
    let onOptionChange = (e) => {
      setLoading(true)
      setLayout(e.value)
    }

    return (
      <div style={{ textAlign: "left" }}>
        <DataViewLayoutOptions layout={layout} onChange={onOptionChange} />
      </div>
    )
  } */

  /* const header = renderHeader() */

  return (
    <div className="dataview-demo">
      <div className="card">
        <DataView
          value={products.map((item, idx) => ({
            ...item,
            avatarUrl: idx % 3,
            severity:
              moment.duration(moment().diff(item.end)).asDays() > 3
                ? "success"
                : "warning",
          }))}
          layout={layout}
          /* header={header} */
          itemTemplate={itemTemplate}
          lazy
          // paginator
          // paginatorPosition={"bottom"}
          // rows={rows.current}
          // totalRecords={totalRecords}
          // first={first}
          // onPage={onPage}
          loading={loading}
        />
      </div>
    </div>
  );
};

export default ProjectsListComponent;
