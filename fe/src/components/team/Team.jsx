import React from "react";
import {
  avatar_1,
  avatar_2,
  avatar_3,
} from "../../assets/img/avatars/index.js";
import profile_4 from "../../assets/img/team/profile_4.jpeg";
import "./Team.css";

const Team = () => {
  const avatars = [avatar_1, avatar_2, avatar_3];

  return (
    <div className="px-4 py-7 md:px-6 lg:px-8">
      <div className="text-900 font-bold text-6xl mb-4 text-center">
        The Team
      </div>
      <div className="text-600 text-xl mb-6 text-center line-height-3">
        What if... you had the financial tools to make your ideas come true?
        With this web-app you can ask for funds worldwide just using your
        Algorand wallet.
      </div>
      <div className="grid">
        <div className="col-12 md:col-4 text-center mb-5">
          <img
            src="https://media-exp1.licdn.com/dms/image/C4D03AQHfB1Fuz8ElxA/profile-displayphoto-shrink_400_400/0/1627296403227?e=1660780800&v=beta&t=FT7h64hrU51OXcnCJ1uubVobNksBNhwQkyzgNWKJr-8"
            alt="avatar-f-1"
            className="team_avatar mb-3"
            width={100}
          />
          <div className="text-xl text-900 font-medium mb-3">
            Vito Nardiello
          </div>
          <div className="text-role text-xl mb-3">Business Analyst</div>
          <div className="inline-flex align-items-center">
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-linkedin text-xl"></i>
            </a>
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-github text-xl"></i>
            </a>
          </div>
        </div>
        <div className="col-12 md:col-4 text-center mb-5">
          <img
            src="https://media-exp1.licdn.com/dms/image/C4E03AQG8NQs6e5Hjrw/profile-displayphoto-shrink_400_400/0/1516864555506?e=1660780800&v=beta&t=xBfZEvzb9OLdbMEbOi3ybPz6UPpvpekMYiIgs6nGe0o"
            alt="avatar-f-2"
            className="team_avatar mb-3"
            width={100}
          />
          <div className="text-xl text-900 font-medium mb-3">
            Alberto DeMezzo
          </div>
          <div className="text-role text-xl mb-3">Business Analyst</div>
          <div className="inline-flex align-items-center">
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-linkedin text-xl"></i>
            </a>
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-github text-xl"></i>
            </a>
          </div>
        </div>
        <div className="col-12 md:col-4 text-center mb-5">
          <img
            src="https://media-exp1.licdn.com/dms/image/C4E03AQEGnjg_mRUzIg/profile-displayphoto-shrink_400_400/0/1616950761453?e=1660780800&v=beta&t=MpRcBLPHK3pz-tY1BqWcHF4Ita3K61-e50h7VJbAfXo"
            alt="avatar-f-3"
            className="team_avatar mb-3"
            width={100}
          />
          <div className="text-xl text-900 font-medium mb-3">
            Riccardo Marino
          </div>
          <div className="text-role text-xl mb-3">Frontend Developer</div>
          <div className="inline-flex align-items-center">
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-linkedin text-xl"></i>
            </a>
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-github text-xl"></i>
            </a>
          </div>
        </div>
        <div className="col-12 md:col-4 text-center mb-5">
          <img
            src={profile_4}
            alt="avatar-f-4"
            className="team_avatar mb-3"
            width={100}
          />
          <div className="text-xl text-900 font-medium mb-3">Chiara Tors</div>
          <div className="text-role text-xl mb-3">Frontend Developer</div>
          <div className="inline-flex align-items-center">
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-linkedin text-xl"></i>
            </a>
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-github text-xl"></i>
            </a>
          </div>
        </div>
        <div className="col-12 md:col-4 text-center mb-5">
          <img
            src={avatar_3}
            alt="avatar-f-5"
            className="team_avatar mb-3"
            width={100}
          />
          <div className="text-xl text-900 font-medium mb-3">Luca Mommi</div>
          <div className="text-role text-xl mb-3">Backend Developer</div>
          <div className="inline-flex align-items-center">
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-linkedin text-xl"></i>
            </a>
            <a className="mr-3 text-600 cursor-pointer">
              <i className="pi pi-github text-xl"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Team;
