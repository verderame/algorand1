import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { InputNumber } from "primereact/inputnumber";
import { InputTextarea } from "primereact/inputtextarea";
import React, { useEffect } from "react";
import { useState } from "react";

export const INPUT_TYPES = {
  textarea: "textarea",
  number: "number",
};

const TextInputDialog = (props) => {
  const {
    onSubmit,
    close,
    visible,
    title,
    inputValue,
    onChangeInput,
    inputType,
    text,
  } = props;

  const [inputElement, setInputElement] = useState(null);
  useEffect(() => {
    switch (inputType) {
      case INPUT_TYPES.textarea:
        setInputElement(
          <InputTextarea
            value={inputValue}
            onChange={onChangeInput}
            style={{ width: "100%", height: "100%" }}
          />
        );
        break;
      case INPUT_TYPES.number:
        setInputElement(
          <InputNumber
            value={inputValue}
            onChange={onChangeInput}
            suffix=" MALGO"
            // min={100000}
            style={{ width: "100%" }}
          />
        );
        break;
      default:
        setInputElement(
          <InputTextarea
            value={inputValue}
            onChange={onChangeInput}
            style={{ width: "100%", height: "100%" }}
          />
        );
        break;
    }
  }, [inputValue]);

  return (
    <Dialog
      header={title}
      visible={visible}
      style={
        inputType === INPUT_TYPES.number
          ? {}
          : {
              width: "70vw",
              height: "70vh",
              maxWidth: "500px",
              minWidth: "300px",
              maxHeight: "500px",
              minHeight: "300px",
            }
      }
      footer={
        <div>
          <Button label="Submit" onClick={onSubmit} />
        </div>
      }
      onHide={close}
    >
      <div className="mb-3">{text}</div>

      {inputElement}
    </Dialog>
  );
};

export default TextInputDialog;
