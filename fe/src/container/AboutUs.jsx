import React, { useState } from "react";
import AppHeader from "../components/app-header/AppHeader";
import Team from "../components/team/Team";
import Partners from "../components/partners/Partners";
// import { connect } from "react-redux";

const AboutUsContainer = (props) => {
  const [mnemonicPhrase, setMnemonicPhrase] = useState("");

  return (
    <div className="app-container">
      <AppHeader
        mnemonicPhrase={mnemonicPhrase}
        setMnemonicPhrase={setMnemonicPhrase}
      />
      <div className="app-container-content">
        <Team />
        <Partners />
      </div>
    </div>
  );
};

// const mapStateToProps = (state) => ({
//   loading: state.userReducer.loading,
// });

// const mapDispatchToProps = (dispatch) => ({
//   fun: () => dispatch(fun()),
// });

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(
AboutUsContainer;
// ));
