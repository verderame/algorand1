import React, { useState } from "react";
import AppHeader from "../components/app-header/AppHeader";
import HeroMain from "../components/hero/HeroMain";
import FeaturedProj from "../components/featured/FeaturedProj";
import ProjectsCarousel from "../components/carousel/ProjectsCarousel";
import Partners from "../components/partners/Partners";
import { Button } from "primereact/button";

const HomeContainer = (props) => {
  const mockProjectList = [
    {
      title: "uno",
      subtitle: "lalalalala",
      imgUrl: "https://www.fillmurray.com/640/360",
      avatarUrl: "0",
      severity: "success",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    },
    {
      title: "due",
      subtitle: "lalalalala",
      imgUrl: "https://www.fillmurray.com/800/567",
      avatarUrl: "1",
      severity: "success",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    },
    {
      title: "tre",
      subtitle: "lalalalala",
      imgUrl: "https://www.fillmurray.com/990/356",
      avatarUrl: "2",
      severity: "warning",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    },
    {
      title: "quattro",
      subtitle: "lalalalala",
      imgUrl: "https://www.fillmurray.com/123/568",
      avatarUrl: "0",
      severity: "danger",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    },
    {
      title: "cinque",
      subtitle: "lalalalala",
      imgUrl: "https://www.fillmurray.com/568/200",
      avatarUrl: "1",
      severity: "danger",
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
    },
  ];

  const mockFeaturedProj = {
    title: "Featured",
    subtitle: "lalalalala",
    imgUrl: "https://www.fillmurray.com/990/356",
    avatarUrl: "2",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ",
  };

  // TODO connect wallet dialog on click on see detail or create
  // TODO connect home page buttons

  const [mnemonicPhrase, setMnemonicPhrase] = useState("");

  return (
    <div className="app-container">
      <AppHeader
        mnemonicPhrase={mnemonicPhrase}
        setMnemonicPhrase={setMnemonicPhrase}
      />
      <div className="app-container-content">
        <HeroMain />
        <FeaturedProj project={mockFeaturedProj} />
        {/* <ProjectsScroller projectList={mockProjectList} /> */}
        <ProjectsCarousel projectList={mockProjectList} />
        <Partners />
      </div>
    </div>
  );
};

// const mapStateToProps = (state) => ({
//   loading: state.userReducer.loading,
// });

// const mapDispatchToProps = (dispatch) => ({
//   fun: () => dispatch(fun()),
// });

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(
HomeContainer;
// ));
