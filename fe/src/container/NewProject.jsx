import React from "react";
import { useState } from "react";
import AppHeader from "../components/app-header/AppHeader";
import { Button } from "primereact/button";
import { useEffect } from "react";
import NewProjectForm from "../components/new-project-form/NewProjectForm";
import create from "../services/create";
import TextInputDialog from "../components/text-input-dialog/TextInputDialog";

const NewProjectContainer = (props) => {
  // const userCtx = useContext(UserContext);

  const [mnemonicPhrase, setMnemonicPhrase] = useState("");

  const [projectData, setProjectData] = useState({
    title: "",
    fundraiseGoal: 0,
    description: "",
    fundraiseStartDate: undefined,
    fundraiseEndDate: undefined,
    projectEndDate: undefined,
    address: "",
  });
  const [creatorMnemonicPhrase, setCreatorMnemonicPhrase] = useState("");
  const [disableSubmit, setDisableSubmit] = useState(true);
  const [showWalletModal, setShowWalletModal] = useState(false);

  // useEffect(() => {
  // }, []);

  useEffect(() => {
    Object.keys(projectData).every((key) => {
      if (!projectData[key]) {
        setDisableSubmit(true);
        return false;
      }
      setDisableSubmit(false);
      return true;
    });

    console.log(projectData);
  }, [projectData]);

  const submitProject = () => {
    create();
  };

  return (
    <div className="app-container">
      <AppHeader
        mnemonicPhrase={mnemonicPhrase}
        setMnemonicPhrase={setMnemonicPhrase}
      />
      <div className="app-container-content pb-7 lg:pb-3">
        <NewProjectForm
          projectData={projectData}
          setProjectData={setProjectData}
        />
        <div className="mx-5 flex justify-content-end align-content-center pb-5 ">
          <span className="my-auto mr-5 text-sm">
            {disableSubmit ? "all fields are mandatory" : "form complete!"}
          </span>
          <Button
            label="submit"
            className="mr-2"
            disabled={disableSubmit}
            onClick={() => create(projectData, mnemonicPhrase)}
            // onClick={() =>
            //   donate(
            //     13,
            //     "3JOMUC4ZKZZ2N5T76XT7IMSIYKHR6VF6NTJXVTD2QNFI57GJUGRT7RK45Q",
            //     20000,
            //     mnemonicPhrase
            //   )
            // }
          />
        </div>
      </div>
      <TextInputDialog
        title="Insert Your Mnemonic Phrase"
        visible={showWalletModal}
        onSubmit={() => create(projectData, mnemonicPhrase)}
        close={() => setShowWalletModal(false)}
        inputValue={creatorMnemonicPhrase}
        onChangeInput={(e) => setMnemonicPhrase(e.target.value)}
      />
    </div>
  );
};

// const mapStateToProps = (state) => ({
//   loading: state.userReducer.loading,
// });

// const mapDispatchToProps = (dispatch) => ({
//   fun: () => dispatch(fun()),
// });

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(
NewProjectContainer;
// ));
