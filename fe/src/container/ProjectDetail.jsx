import moment from "moment";
import { Button } from "primereact/button";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import AppHeader from "../components/app-header/AppHeader";
import NewProjectForm from "../components/new-project-form/NewProjectForm";
import TextInputDialog, {
  INPUT_TYPES,
} from "../components/text-input-dialog/TextInputDialog";
import donate from "../services/donate";
import getProjectDetail from "../services/project-detail";
// import { connect } from "react-redux";

const ProjectDetailContainer = (props) => {
  const id = parseInt(useParams().id);
  const [mnemonicPhrase, setMnemonicPhrase] = useState("");

  const [projectData, setProjectData] = useState({
    title: "",
    escrow: "",
    fundraiseGoal: 0,
    description: "",
    fundraise: 0,
    fundraiseStartDate: undefined,
    fundraiseEndDate: undefined,
    projectEndDate: undefined,
    address: "",
  });
  const [creatorMnemonicPhrase, setCreatorMnemonicPhrase] = useState("");
  const [donation, setDonation] = useState(0);
  const [disableSubmit, setDisableSubmit] = useState(true);
  const [showWalletModal, setShowWalletModal] = useState(false);
  const [showDonateModal, setShowDonatetModal] = useState(false);

  useEffect(() => {
    getProjectDetail(id, mnemonicPhrase).then((res) => {
      let details = res;
      setProjectData({
        title: details.title,
        escrow: details.Escrow,
        fundraiseGoal: details.Goal,
        description: details.description,
        fundraiseStartDate: new Date(details.StartDate * 1000),
        fundraiseEndDate: new Date(details.FundCloseDate * 1000),
        projectEndDate: new Date(details.EndDate * 1000),
        fundraise: details.Total,
        address: details.Receiver,
      });
    });
  }, []);

  useEffect(() => {
    Object.keys(projectData).every((key) => {
      if (!projectData[key]) {
        setDisableSubmit(true);
        return false;
      }
      setDisableSubmit(false);
      return true;
    });

    console.log(projectData);
  }, [projectData]);

  return (
    <div className="app-container">
      <AppHeader
        mnemonicPhrase={mnemonicPhrase}
        setMnemonicPhrase={setMnemonicPhrase}
      />
      <div className="app-container-content pb-7 lg:pb-3">
        <NewProjectForm
          disabled
          projectData={projectData}
          setProjectData={setProjectData}
        />
        <div className="mx-5 flex justify-content-end align-content-center pb-5">
          <Button
            label="Donate"
            icon="pi pi-heart"
            className="mr-2"
            onClick={() => setShowDonatetModal(true)}
          />
        </div>
      </div>
      <TextInputDialog
        title="Insert Your Mnemonic Phrase"
        visible={showWalletModal}
        // onSubmit={() => create(projectData, mnemonicPhrase)}
        close={() => setShowWalletModal(false)}
        inputValue={creatorMnemonicPhrase}
        onChangeInput={(e) => setMnemonicPhrase(e.target.value)}
      />
      <TextInputDialog
        title="How much do you want to donate for this project?"
        text="(100000 Minimum)"
        visible={showDonateModal}
        inputType={INPUT_TYPES.number}
        onSubmit={() =>
          donate(id, projectData.escrow, donation, mnemonicPhrase)
        }
        close={() => setShowDonatetModal(false)}
        inputValue={donation}
        onChangeInput={(e) => setDonation(e.value)}
      />
    </div>
  );
};

// const mapStateToProps = (state) => ({
//   loading: state.userReducer.loading,
// });

// const mapDispatchToProps = (dispatch) => ({
//   fun: () => dispatch(fun()),
// });

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(
ProjectDetailContainer;
// ));
