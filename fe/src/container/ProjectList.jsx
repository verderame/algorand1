import React, { useState } from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AppHeader from "../components/app-header/AppHeader";
import ProjectsListComponent from "../components/projects-list/ProjectsListComponent";
import getAllProjects from "../services/project-list";
// import { connect } from "react-redux";

const ProjectListContainer = (props) => {
  const navigate = useNavigate();
  const [projectList, setProjectList] = useState([]);
  const [loading, setLoading] = useState(true);
  const [mnemonicPhrase, setMnemonicPhrase] = useState("");

  useEffect(() => {
    getAllProjects().then((res) => {
      if (res.data) {
        setProjectList(res.data);
        console.log(res.data);
        setLoading(false);
      }
    });
  }, []);

  return (
    <div className="app-container">
      <AppHeader
        mnemonicPhrase={mnemonicPhrase}
        setMnemonicPhrase={setMnemonicPhrase}
      />
      <div className="app-container-content">
        <div className="px-4 py-7 md:px-6 lg:px-8">
          <div className="text-900 font-bold text-6xl mb-4 text-center">
            All Projects
          </div>
          <div className="text-600 text-xl mb-6 text-center line-height-3">
            And you? Try to make your dreams come true.
          </div>
          <ProjectsListComponent
            products={projectList}
            loading={loading}
            goToDetail={(id) => navigate(`/detail/${id}`)}
          />
        </div>
      </div>
    </div>
  );
};

// const mapStateToProps = (state) => ({
//   loading: state.userReducer.loading,
// });

// const mapDispatchToProps = (dispatch) => ({
//   fun: () => dispatch(fun()),
// });

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(withRouter(
ProjectListContainer;
// ));
