import { LogicSigAccount } from "algosdk";
import moment from "moment";
import {
  client,
  algosdk,
  CLEAN_PROGRAM_BYTES,
  APPROVAL_PROGRAM_BYTES,
  http,
  baseURL8080,
  numberToBytes,
} from "./commons";

async function waitForConfirmation(algodclient, txId, timeout) {
  // Wait until the transaction is confirmed or rejected, or until 'timeout'
  // number of rounds have passed.
  //     Args:
  // txId(str): the transaction to wait for
  // timeout(int): maximum number of rounds to wait
  // Returns:
  // pending transaction information, or throws an error if the transaction
  // is not confirmed or rejected in the next timeout rounds
  if (algodclient == null || txId == null || timeout < 0) {
    throw new Error("Bad arguments.");
  }
  const status = await algodclient.status().do();
  if (typeof status === "undefined")
    throw new Error("Unable to get node status");
  const startround = status["last-round"] + 1;
  let currentround = startround;

  /* eslint-disable no-await-in-loop */
  while (currentround < startround + timeout) {
    const pendingInfo = await algodclient
      .pendingTransactionInformation(txId)
      .do();
    if (pendingInfo !== undefined) {
      if (
        pendingInfo["confirmed-round"] !== null &&
        pendingInfo["confirmed-round"] > 0
      ) {
        // Got the completed Transaction
        return pendingInfo;
      }

      if (
        pendingInfo["pool-error"] != null &&
        pendingInfo["pool-error"].length > 0
      ) {
        // If there was a pool error, then the transaction has been rejected!
        throw new Error(
          `Transaction Rejected pool error${pendingInfo["pool-error"]}`
        );
      }
    }
    await algodclient.statusAfterBlock(currentround).do();
    currentround += 1;
  }
  /* eslint-enable no-await-in-loop */
  throw new Error(`Transaction not confirmed after ${timeout} rounds!`);
}

const saveProject = (args) => {
  return http.post(baseURL8080, args);
};

const create = async (params, creatorMnemonicPhrase) => {
  const address = params.address;

  // check receiver address
  if (!algosdk.isValidAddress(address)) {
    alert("address not valid!");
    return;
  }

  // create fundraise info smartcontract
  let suggestedParams;
  client
    .getTransactionParams()
    .do()
    .then(
      async (res) => {
        suggestedParams = res;

        // create application with arguments
        const start = moment(params.fundraiseStartDate)
          .startOf("day")
          .unix();
        const close = moment(params.fundraiseEndDate).unix();
        const end = moment(params.projectEndDate).unix();

        const goal = params.fundraiseGoal;

        // const appArg0 = new Uint8Array(Buffer.from(start, "ascii"));
        // const appArg1 = new Uint8Array(Buffer.from(end, "ascii"));
        // const appArg2 = new Uint8Array(Buffer.from(goal, "ascii"));
        // const appArg3 = new Uint8Array(Buffer.from(address, "base64"));
        // const appArg4 = new Uint8Array(Buffer.from(close, "ascii"));

        // const appArg0 = algosdk.encodeUint64(start);
        // const appArg1 = algosdk.encodeUint64(end);
        // const appArg2 = algosdk.encodeUint64(goal);
        // const appArg3 = new Uint8Array(Buffer.from(address));
        // const appArg4 = algosdk.encodeUint64(close);

        const apb = await getApprovalProgramBytes({
          start: start,
          end: end,
          goal: goal,
          close: close,
        });
        const approvalProgramBytes = new Uint8Array(Buffer.from(apb, "base64"));
        const cleanProgramBytes = new Uint8Array(
          Buffer.from(CLEAN_PROGRAM_BYTES, "base64")
        );

        // const appArgs = [appArg0, appArg1, appArg2, appArg3, appArg4];
        const appArgs = [algosdk.decodeAddress(address).publicKey];
        // console.log("appArgs", appArgs);
        const account = algosdk.mnemonicToSecretKey(creatorMnemonicPhrase);
        console.log("account", account);
        const unsignedTransaction = algosdk.makeApplicationCreateTxn(
          account.addr,
          suggestedParams,
          algosdk.OnApplicationComplete.NoOpOC,
          approvalProgramBytes,
          cleanProgramBytes,
          1,
          0,
          5,
          3,
          appArgs
        );
        console.log("unsignedTransaction", unsignedTransaction);
        const signedTransaction = unsignedTransaction.signTxn(
          new Uint8Array(Buffer.from(account.sk, "base64"))
        );
        console.log("signedTransaction", signedTransaction);
        client
          .sendRawTransaction(signedTransaction)
          .do()
          .then(
            (res) => {
              let transactionId = res.txId;
              console.log("transactionId", transactionId);
              waitForConfirmation(client, transactionId, 2).then(
                (res) => {
                  let appId = res["application-index"];
                  console.log("confirmation", res);

                  // create money handler smart contract

                  getEscrowBytes(appId).then((escrowBytes) => {
                    const escrowCreation = new algosdk.LogicSigAccount(
                      escrowBytes
                    );
                    let escrowAddress = escrowCreation.address();
                    console.log("appId", appId, "escrowAddress", escrowAddress);

                    // transaction to update the app and add
                    const escrowUnsignedTransaction = algosdk.makeApplicationUpdateTxn(
                      account.addr,
                      suggestedParams,
                      appId,
                      approvalProgramBytes,
                      cleanProgramBytes,
                      [algosdk.decodeAddress(escrowAddress).publicKey]
                    );

                    console.log("account qui", account);
                    const escrowSignedTransaction = escrowUnsignedTransaction.signTxn(
                      new Uint8Array(Buffer.from(account.sk, "base64"))
                    );

                    client
                      .sendRawTransaction(escrowSignedTransaction)
                      .do()
                      .then(
                        (res) => console.log("transaction eskere", res),
                        (err) => console.log(err)
                      );

                    // save project details on off-chain DB
                    saveProject({
                      appId: appId,
                      name: params.title,
                      description: params.description,
                      start: params.fundraiseStartDate.toUTCString(),
                      end: params.fundraiseEndDate.toUTCString(),
                      escrow: escrowAddress,
                    }).then((res) => console.log("app saved", res));
                  });
                },
                (err) => {
                  console.log(err);
                }
              );
            },
            (err) => console.log(err)
          );
      },
      (err) => console.log(err)
    );
};

async function getEscrowBytes(appId) {
  const program =
    "#pragma version 2\n" +
    "\n" +
    "global GroupSize\n" +
    "int 2\n" +
    "==\n" +
    "gtxn 0 TypeEnum\n" +
    "int 6\n" +
    "==\n" +
    "&&\n" +
    "gtxn 0 ApplicationID\n" +
    "int APP_ID\n" +
    "==\n" +
    "&&\n" +
    "gtxn 0 OnCompletion\n" +
    "int NoOp\n" +
    "==\n" +
    "int DeleteApplication\n" +
    "gtxn 0 OnCompletion\n" +
    "==\n" +
    "||\n" +
    "&&\n" +
    "gtxn 1 RekeyTo\n" +
    "global ZeroAddress\n" +
    "==\n" +
    "&&\n" +
    "gtxn 0 RekeyTo\n" +
    "global ZeroAddress\n" +
    "==\n" +
    "&&\n";

  // use algod to compile the program
  const compiledProgram = await client
    .compile(program.replace("APP_ID", appId))
    .do();
  return new Uint8Array(Buffer.from(compiledProgram.result, "base64"));
}

export async function getApprovalProgramBytes(params) {
  const program =
    "#pragma version 2\n" +
    "// Fund Start Date unixtimestamp\n" +
    "// Fund End Date unixtimestamp\n" +
    "// Fund Goal\n" +
    "// Fund Amount - total\n" +
    "// Escrow Address\n" +
    "// Creator Address\n" +
    "\n" +
    "// check if the app is being created\n" +
    "// if so save creator\n" +
    "int 0\n" +
    "txn ApplicationID\n" +
    "==\n" +
    "bz not_creation\n" +
    'byte "Creator"\n' +
    "txn Sender\n" +
    "app_global_put\n" +
    "//5 args on creation\n" +
    "// transaction will fail\n" +
    "// if 5 args are not passed during creation\n" +
    "txn NumAppArgs\n" +
    "int 1\n" +
    "==\n" +
    "bz failed\n" +
    'byte "StartDate"\n' +
    `int ${params.start}\n` +
    "app_global_put\n" +
    'byte "EndDate"\n' +
    `int ${params.end}\n` +
    "app_global_put\n" +
    'byte "Goal"\n' +
    `int ${params.goal}\n` +
    "app_global_put\n" +
    'byte "Receiver"\n' +
    `txna ApplicationArgs 0\n` +
    "app_global_put\n" +
    'byte "Total"\n' +
    "int 0\n" +
    "app_global_put\n" +
    'byte "FundCloseDate"\n' +
    `int ${params.close}\n` +
    "app_global_put\n" +
    "int 1\n" +
    "return\n" +
    "not_creation:\n" +
    "// check if this is deletion transaction\n" +
    "int DeleteApplication\n" +
    "txn OnCompletion\n" +
    "==\n" +
    "bz not_deletion\n" +
    "// To delete the app\n" +
    "// The creator must empty the escrow\n" +
    "// to the fund receiver if the escrow has funds\n" +
    "// only the creator\n" +
    "// can delete the app\n" +
    'byte "Creator"\n' +
    "app_global_get\n" +
    "txn Sender\n" +
    "==\n" +
    "// check that we are past fund close date\n" +
    "global LatestTimestamp\n" +
    'byte "FundCloseDate"\n' +
    "app_global_get\n" +
    ">=\n" +
    "&&\n" +
    "bz failed\n" +
    "// if escrow balance is zero\n" +
    "// let the app be deleted\n" +
    "// escrow account must be passed\n" +
    "// into the call as an argument\n" +
    "int 0\n" +
    "int 1\n" +
    "balance\n" +
    "==\n" +
    "// if the balance is 0 allow the delete\n" +
    "bnz finished\n" +
    "// if the escrow is not empty then\n" +
    "// there must be need two transactions\n" +
    "// in a group\n" +
    "global GroupSize\n" +
    "int 2\n" +
    "==\n" +
    "// second tx is an payment\n" +
    "gtxn 1 TypeEnum\n" +
    "int 1\n" +
    "==\n" +
    "&&\n" +
    "// the second payment transaction should be\n" +
    "// a close out transaction to receiver\n" +
    'byte "Receiver"\n' +
    "app_global_get\n" +
    "gtxn 1 CloseRemainderTo\n" +
    "==\n" +
    "&&\n" +
    "// the amount of the payment transaction\n" +
    "// should be 0\n" +
    "gtxn 1 Amount\n" +
    "int 0\n" +
    "==\n" +
    "&&\n" +
    "// the sender of the payment transaction\n" +
    "// should be the escrow account\n" +
    'byte "Escrow"\n' +
    "app_global_get\n" +
    "gtxn 1 Sender\n" +
    "==\n" +
    "&&\n" +
    "bz failed\n" +
    "int 1\n" +
    "return\n" +
    "not_deletion:\n" +
    "//---\n" +
    "// check if this is update ---\n" +
    "int UpdateApplication\n" +
    "txn OnCompletion\n" +
    "==\n" +
    "bz not_update\n" +
    "// verify that the creator is\n" +
    "// making the call\n" +
    'byte "Creator"\n' +
    "app_global_get\n" +
    "txn Sender\n" +
    "==\n" +
    "// the call should pass the escrow\n" +
    "// address\n" +
    "txn NumAppArgs\n" +
    "int 1\n" +
    "==\n" +
    "&&\n" +
    "bz failed\n" +
    "// store the address in global state\n" +
    "// this parameter should be addr:\n" +
    'byte "Escrow"\n' +
    "txna ApplicationArgs 0\n" +
    "app_global_put\n" +
    "int 1\n" +
    "return\n" +
    "not_update:\n" +
    "//---\n" +
    "// check for closeout\n" +
    "int CloseOut\n" +
    "txn OnCompletion\n" +
    "==\n" +
    "bnz close_out\n" +
    "// check if no params are\n" +
    "// passed in, which should\n" +
    "// only happen when someone just\n" +
    "// wants to optin\n" +
    "// note the code is written\n" +
    "// to allow opting in and donating with\n" +
    "// one call\n" +
    "int 0\n" +
    "txn NumAppArgs\n" +
    "==\n" +
    "bz check_parms\n" +
    "// Verify someone is\n" +
    "// not just opting in\n" +
    "int OptIn\n" +
    "txn OnCompletion\n" +
    "==\n" +
    "bz failed\n" +
    "int 1\n" +
    "return\n" +
    "check_parms:\n" +
    "// donate\n" +
    "txna ApplicationArgs 0\n" +
    'byte "donate"\n' +
    "==\n" +
    "bnz donate\n" +
    "// reclaim\n" +
    "txna ApplicationArgs 0\n" +
    'byte "reclaim"\n' +
    "==\n" +
    "bnz reclaim\n" +
    "// claim\n" +
    "txna ApplicationArgs 0\n" +
    'byte "claim"\n' +
    "==\n" +
    "bnz claim\n" +
    "b failed\n" +
    "\n" +
    "donate:\n" +
    "// check dates to verify\n" +
    "// in valid range\n" +
    "global LatestTimestamp\n" +
    'byte "StartDate"\n' +
    "app_global_get\n" +
    ">=\n" +
    "global LatestTimestamp\n" +
    'byte "EndDate"\n' +
    "app_global_get\n" +
    "<=\n" +
    "&&\n" +
    "bz failed\n" +
    "// check if grouped with\n" +
    "// two transactions\n" +
    "global GroupSize\n" +
    "int 2\n" +
    "==\n" +
    "// second tx is a payment\n" +
    "gtxn 1 TypeEnum\n" +
    "int 1\n" +
    "==\n" +
    "&&\n" +
    "bz failed\n" +
    "// verify escrow is receiving\n" +
    "// second payment tx\n" +
    'byte "Escrow"\n' +
    "app_global_get\n" +
    "gtxn 1 Receiver\n" +
    "==\n" +
    "bz failed\n" +
    "// increment the total\n" +
    "// funds raised so far\n" +
    'byte "Total"\n' +
    "app_global_get\n" +
    "gtxn 1 Amount\n" +
    "+\n" +
    "store 1\n" +
    'byte "Total"\n' +
    "load 1\n" +
    "app_global_put\n" +
    "// increment or set giving amount\n" +
    "// for the account that is donating\n" +
    "int 0 //sender\n" +
    "txn ApplicationID\n" +
    'byte "MyAmountGiven"\n' +
    "app_local_get_ex\n" +
    "// check if a new giver\n" +
    "// or existing giver\n" +
    "// and store the value\n" +
    "// in the givers local storage\n" +
    "bz new_giver\n" +
    "gtxn 1 Amount\n" +
    "+\n" +
    "store 3\n" +
    "int 0 //sender\n" +
    'byte "MyAmountGiven"\n' +
    "load 3\n" +
    "app_local_put\n" +
    "b finished\n" +
    "new_giver:\n" +
    "int 0 //sender\n" +
    'byte "MyAmountGiven"\n' +
    "gtxn 1 Amount\n" +
    "app_local_put\n" +
    "b finished\n" +
    "\n" +
    "\n" +
    "\n" +
    "claim:\n" +
    "// verify there are 2 transactions\n" +
    "// in the group\n" +
    "global GroupSize\n" +
    "int 2\n" +
    "==\n" +
    "bz failed\n" +
    "// verify that the receiver\n" +
    "// of the payment transaction\n" +
    "// is the address stored\n" +
    "// when the fund was created\n" +
    "gtxn 1 Receiver\n" +
    'byte "Receiver"\n' +
    "app_global_get\n" +
    "==\n" +
    "// verify the sender\n" +
    "// of the payment transaction\n" +
    "// is the escrow account\n" +
    "gtxn 1 Sender\n" +
    'byte "Escrow"\n' +
    "app_global_get\n" +
    "==\n" +
    "&&\n" +
    "// verify that the CloseRemainderTo\n" +
    "// attribute is set to the receiver\n" +
    "gtxn 1 CloseRemainderTo\n" +
    'byte "Receiver"\n' +
    "app_global_get\n" +
    "==\n" +
    "&&\n" +
    "// verify that the fund end date\n" +
    "// has passed\n" +
    "global LatestTimestamp\n" +
    'byte "EndDate"\n' +
    "app_global_get\n" +
    ">\n" +
    "&&\n" +
    "bz failed\n" +
    "// verify that the goal was reached\n" +
    'byte "Total"\n' +
    "app_global_get\n" +
    'byte "Goal"\n' +
    "app_global_get\n" +
    ">=\n" +
    "bz failed\n" +
    "b finished\n" +
    "//dont check amount because\n" +
    "//escrow account may receiver\n" +
    "//more than the goal\n" +
    "//Could check for total vs tx1 amount\n" +
    "\n" +
    "//Fund did not meet total\n" +
    "//Allow ppl to reclaim funds\n" +
    "//may be an issue when the last\n" +
    "//person relcaims because of min balance\n" +
    "//without a closeto\n" +
    "// only 1 relcaim tx is allowed per account\n" +
    "reclaim:\n" +
    "// verify there are 2 transactions\n" +
    "// in the group\n" +
    "global GroupSize\n" +
    "int 2\n" +
    "==\n" +
    "bz failed\n" +
    "// verfiy that smart contract\n" +
    "// caller is the payment\n" +
    "// transction receiver\n" +
    "gtxn 1 Receiver\n" +
    "gtxn 0 Sender\n" +
    "==\n" +
    "// Verify that payment\n" +
    "// transaction is from the escrow\n" +
    "gtxn 1 Sender\n" +
    'byte "Escrow"\n' +
    "app_global_get\n" +
    "==\n" +
    "&&\n" +
    "// verify that fund end date has passed\n" +
    "global LatestTimestamp\n" +
    'byte "EndDate"\n' +
    "app_global_get\n" +
    ">\n" +
    "&&\n" +
    "// verify the fund goal was\n" +
    "// not met\n" +
    'byte "Total"\n' +
    "app_global_get\n" +
    'byte "Goal"\n' +
    "app_global_get\n" +
    "<\n" +
    "&&\n" +
    "// because the escrow\n" +
    "// has to pay the fee\n" +
    "// the amount of the\n" +
    "// payment transaction\n" +
    "// plus the fee should\n" +
    "// be less than or equal to\n" +
    "// the amount originally\n" +
    "// given\n" +
    "gtxn 1 Amount\n" +
    "gtxn 1 Fee\n" +
    "+\n" +
    "int 0\n" +
    'byte "MyAmountGiven"\n' +
    "app_local_get\n" +
    "<=\n" +
    "&&\n" +
    "bz failed\n" +
    "//check the escrow account total\n" +
    "//--app-account for the escrow\n" +
    "// needs to pass the address\n" +
    "// of the escrow\n" +
    "// check that this is the\n" +
    "// last recoverd donation\n" +
    "// if it is the closeremainderto\n" +
    "// should be set\n" +
    "gtxn 1 Fee\n" +
    "gtxn 1 Amount\n" +
    "+\n" +
    "// the int 1 is the ref to escrow\n" +
    "int 1\n" +
    "balance\n" +
    "==\n" +
    "gtxn 1 CloseRemainderTo\n" +
    "global ZeroAddress\n" +
    "==\n" +
    "||\n" +
    "bz failed\n" +
    "// decrement the given amount\n" +
    "// of the sender\n" +
    "int 0\n" +
    'byte "MyAmountGiven"\n' +
    "app_local_get\n" +
    "gtxn 1 Amount\n" +
    "-\n" +
    "gtxn 1 Fee\n" +
    "-\n" +
    "store 5\n" +
    "int 0\n" +
    'byte "MyAmountGiven"\n' +
    "load 5\n" +
    "app_local_put\n" +
    "b finished\n" +
    "//call if this is a closeout op\n" +
    "close_out:\n" +
    "int 1\n" +
    "return\n" +
    "failed:\n" +
    "int 0\n" +
    "return\n" +
    "finished:\n" +
    "int 1\n" +
    "return\n";

  // use algod to compile the program
  const compiledProgram = await client.compile(program).do();
  return new Uint8Array(Buffer.from(compiledProgram.result, "base64"));
}

export default create;
