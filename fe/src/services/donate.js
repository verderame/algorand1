import { client, algosdk } from "./commons";

async function waitForConfirmation(algodclient, txId, timeout) {
  // Wait until the transaction is confirmed or rejected, or until 'timeout'
  // number of rounds have passed.
  //     Args:
  // txId(str): the transaction to wait for
  // timeout(int): maximum number of rounds to wait
  // Returns:
  // pending transaction information, or throws an error if the transaction
  // is not confirmed or rejected in the next timeout rounds
  if (algodclient == null || txId == null || timeout < 0) {
    throw new Error("Bad arguments.");
  }
  const status = await algodclient.status().do();
  if (typeof status === "undefined")
    throw new Error("Unable to get node status");
  const startround = status["last-round"] + 1;
  let currentround = startround;

  /* eslint-disable no-await-in-loop */
  while (currentround < startround + timeout) {
    const pendingInfo = await algodclient
      .pendingTransactionInformation(txId)
      .do();
    if (pendingInfo !== undefined) {
      if (
        pendingInfo["confirmed-round"] !== null &&
        pendingInfo["confirmed-round"] > 0
      ) {
        // Got the completed Transaction
        return pendingInfo;
      }

      if (
        pendingInfo["pool-error"] != null &&
        pendingInfo["pool-error"].length > 0
      ) {
        // If there was a pool error, then the transaction has been rejected!
        throw new Error(
          `Transaction Rejected pool error${pendingInfo["pool-error"]}`
        );
      }
    }
    await algodclient.statusAfterBlock(currentround).do();
    currentround += 1;
  }
  /* eslint-enable no-await-in-loop */
  throw new Error(`Transaction not confirmed after ${timeout} rounds!`);
}

const donate = (appId, escrowAddress, amount, senderMnemonicPhrase) => {
  const senderAccount = algosdk.mnemonicToSecretKey(senderMnemonicPhrase);

  let suggestedParams;
  client
    .getTransactionParams()
    .do()
    .then(
      (res) => {
        suggestedParams = res;

        // opt in to the created application
        const optInTxn = algosdk.makeApplicationOptInTxn(
          senderAccount.addr,
          suggestedParams,
          appId
        );

        const signedOptInTxn = optInTxn.signTxn(senderAccount.sk);

        let optInTxnId;
        client
          .sendRawTransaction(signedOptInTxn)
          .do()
          .then((res) => {
            optInTxnId = res.txId;
            // wait for confirmation
            let completedTx;
            algosdk.waitForConfirmation(client, optInTxnId, 4).then((res) => {
              completedTx = res;
              console.log("opt in complete", completedTx);

              // create the donate transaction
              const donateTxn = algosdk.makeApplicationCallTxnFromObject({
                appIndex: appId,
                from: senderAccount.addr,
                onComplete: algosdk.OnApplicationComplete.NoOpOC,
                appArgs: [new Uint8Array(Buffer.from("donate"))],
                suggestedParams: suggestedParams,
              });

              // create the escrow transaction for the donation
              const escrowTxn = algosdk.makePaymentTxnWithSuggestedParams(
                senderAccount.addr,
                escrowAddress,
                amount,
                undefined,
                undefined,
                suggestedParams
              );

              // group transactions
              const txnGroup = [donateTxn, escrowTxn];
              const txnGroupId = algosdk.assignGroupID(txnGroup);

              // sign the transactions
              const signedDonateTxn = donateTxn.signTxn(senderAccount.sk);
              const signedEscrowTxn = escrowTxn.signTxn(senderAccount.sk);

              const signedGroupTxn = [signedDonateTxn, signedEscrowTxn];
              console.log("signed txn group", signedGroupTxn);

              // send transaction group
              let groupTxnId;
              client
                .sendRawTransaction(signedGroupTxn)
                .do()
                .then((res) => {
                  groupTxnId = res.txId;
                  // wait for confirmation
                  let completedGroupTx;
                  algosdk
                    .waitForConfirmation(client, groupTxnId, 4)
                    .then((res) => {
                      completedGroupTx = res;
                      console.log(
                        "donate & escrow donation group complete",
                        completedGroupTx
                      );

                      // closeout transaction
                      const closeOutTxn = algosdk.makeApplicationCloseOutTxn(
                        senderAccount.addr,
                        suggestedParams,
                        appId
                      );

                      const signedCloseOutTxn = closeOutTxn.signTxn(
                        senderAccount.sk
                      );

                      client
                        .sendRawTransaction(signedCloseOutTxn)
                        .do()
                        .then((res) => {
                          let closeOutTxnId = res.txId;
                          let completedTx;
                          algosdk
                            .waitForConfirmation(client, closeOutTxnId, 4)
                            .then((res) => {
                              completedTx = res;
                              console.log("closeout complete", completedTx);
                            });
                        });
                    });
                });
            });
          });
      },
      (err) => console.log(err)
    );
};

export default donate;
