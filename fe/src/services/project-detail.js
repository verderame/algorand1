import { algosdk, baseURL8080, client, http } from "./commons";

export const getProjectInfo = (id) => {
  return http.get(`${baseURL8080}/${id}`);
};

export const getProjectFromBlockChain = async (id, viewerMnemonicPhrase) => {
  const viewerAccount = algosdk.mnemonicToSecretKey(viewerMnemonicPhrase);

  let projectInfo = {};
  let applicationInfoResponse = await client
    .accountApplicationInformation(viewerAccount.addr, id)
    .do();

  let globalState = [];
  if (applicationInfoResponse["created-app"]["global-state"]) {
    globalState = applicationInfoResponse["created-app"]["global-state"];
    console.log("qua", globalState);
  }
  for (let n = 0; n < globalState.length; n++) {
    projectInfo[atob(globalState[n].key)] = globalState[n].value.bytes
      ? globalState[n].value.bytes
      : globalState[n].value.uint;
    console.log("globalstate", atob(globalState[n].key, globalState[n].value));
  }

  return projectInfo;
};

const getProjectDetail = async (id, viewerMnemonicPhrase) => {
  const res = await getProjectInfo(id);
  let details;
  if (res.data) {
    details = {
      title: res.data.name,
      escrow: res.data.escrow,
      description: res.data.description,
    };
  }

  // TODO test creare una app con creator != receiver
  const otherDetails = await getProjectFromBlockChain(id, viewerMnemonicPhrase);
  details = {
    ...details,
    ...otherDetails,
    Escrow: algosdk.encodeAddress(
      new Uint8Array(Buffer.from(otherDetails.Escrow.toString(), "base64"))
    ),
    Receiver: algosdk.encodeAddress(
      new Uint8Array(Buffer.from(otherDetails.Receiver.toString(), "base64"))
    ),
    Creator: algosdk.encodeAddress(
      new Uint8Array(Buffer.from(otherDetails.Creator.toString(), "base64"))
    ),
  };

  return details;
};

export default getProjectDetail;
