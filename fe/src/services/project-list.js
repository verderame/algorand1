import { baseURL8080, http } from "./commons";

const getAllProjects = () => {
  return http.get(baseURL8080);
};

export default getAllProjects;
