import React from "react";

const UserContext = React.useContext({
  mnemonicPhrase: "",
});

export default UserContext;
